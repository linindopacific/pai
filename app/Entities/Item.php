<?php namespace App\Entities;

use CodeIgniter\Entity;

class Item extends Entity
{
    protected $casts = [];

    public function getSoh()
    {
        $itemModel = new \App\Models\LedgerEntryModel();
        $builder = $itemModel->builder();

        $q = $builder->selectSum('quantity', 'soh')->where('item_no', $this->attributes['no'])->get()->getRowArray();
        return (is_null($q['soh'])) ? (float)0 : (float)$q['soh'];
    }
}