<?= $this->extend($viewLayout) ?>
<?= $this->section("pageStyles") ?>
<link rel="stylesheet" href="https://unpkg.com/purecss@2.0.5/build/tables-min.css" />
<?= $this->endSection() ?>

<?= $this->section("content") ?>
<section>
	<?php

		$table = new \CodeIgniter\View\Table();
		$template = [
			'table_open' => '<table class="pure-table pure-table-bordered">'
		];
		$table->setTemplate($template);
		$table->setHeading('itemNo', 'description', 'baseUom', 'soh');
		foreach ($items as $row)
		{
			$table->addRow($row->no, $row->description, $row->base_uom, $row->soh);
		}
		echo $table->generate();
	?>

</section>
<?= $this->endSection() ?>

<?= $this->section("pageScripts") ?>
<?= $this->endSection() ?>