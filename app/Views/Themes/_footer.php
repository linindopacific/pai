<footer>
	<!--div class="environment">
		<p>Page rendered in {elapsed_time} seconds</p>
		<p>Environment: <?= ENVIRONMENT ?></p>
	</div-->

	<div class="copyrights">
		<p>&copy; <?= date('Y') ?> PAI.</p>
	</div>

</footer>