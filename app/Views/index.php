<?= $this->extend($viewLayout) ?>
<?= $this->section("pageStyles") ?>
<?= $this->endSection() ?>

<?= $this->section("content") ?>
<section>
	<h1><?= $title; ?></h1>
</section>
<?= $this->endSection() ?>

<?= $this->section("pageScripts") ?>
<?= $this->endSection() ?>