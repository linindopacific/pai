<?= $this->extend($viewLayout) ?>
<?= $this->section("pageStyles") ?>
<link rel="stylesheet" href="https://unpkg.com/purecss@2.0.5/build/forms-nr-min.css" />
<link rel="stylesheet" href="<?= base_url('public/css/auto-complete.css') ?>" />
<?= $this->endSection() ?>

<?= $this->section("content") ?>
<section>
	<h3>You're logged in as <?= auth()->user()->username ?></h3>
	<?php //echo $validation->listErrors() ?>
	<!-- Validation Errors and Message -->
	<?= $this->include('App\Views\Auth\messages') ?>
	<br>
	<div>
	<?= form_open('postLedger', ['class' => 'pure-form pure-form-stacked']); ?>
	<?= csrf_field() ?>
		<fieldset>
			<legend>Entry Form</legend>
			<label for="stacked-item_no"><?= lang('Validation.item_no') ?></label>
			<input name="item_no" type="text" class="" id="stacked-item_no" placeholder="<?= lang('Validation.item_no') ?>" value="<?= old('item_no') ?>" autofocus="" />
			<?php // echo $validation->showError('item_no', 'my_single') ?>

			<label for="stacked-quantity"><?= lang('Validation.quantity') ?></label>
			<input name="quantity" type="number" class="" id="stacked-quantity" placeholder="<?= lang('Validation.quantity') ?>" min="1" value="<?= old('quantity') ?>" />
			<?php // echo $validation->showError('quantity', 'my_single') ?>

			<label for="radio-option-one" class="pure-radio">
				<input type="radio" id="radio-option-one" name="opt" value="out" <?= old('opt') == 'out' ? 'checked' : '' ?> required /> Outbound</label>
			<label for="radio-option-two" class="pure-radio">
				<input type="radio" id="radio-option-two" name="opt" value="in" <?= old('opt') == 'in' ? 'checked' : '' ?> required /> Inbound</label>

			<label for="stacked-external_document_no"><?= lang('Validation.external_document_no') ?></label>
			<input name="external_document_no" type="text" class="" id="stacked-external_document_no" placeholder="<?= lang('Validation.external_document_no') ?>" value="<?= old('external_document_no') ?>" />
			<?php // echo $validation->showError('external_document_no', 'my_single') ?>
			<br>
			<button type="submit" class="pure-button pure-button-primary">Post</button>
		</fieldset>
	<?= form_close() ?>
	</div>
</section>
<?= $this->endSection() ?>

<?= $this->section("pageScripts") ?>
<script src="<?= base_url('public/js/auto-complete.min.js') ?>"></script>
<script>
	var demo1 = new autoComplete({
		selector: 'input[name="item_no"]',
		minChars: 1,
		source: function(term, response){
			$.getJSON('<?= site_url('item/search') ?>', { item_no: term }, function(data){ response(data); });
		}
	});
</script>
<?= $this->endSection() ?>