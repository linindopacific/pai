<?php namespace App\Controllers;

use Fluent\Auth\Config\Services;

class LedgerEntryController extends BaseController
{
    public function attemptPost()
    {
        $serviceFulfillmentModel = new \App\Models\LedgerEntryModel();
        $validation =  \Config\Services::validation();
		$rules = $serviceFulfillmentModel->getValidationRules(['except' => ['uom']]);
		$validationResult = $validation->setRules($rules)->run($this->request->getPost());
		if ( !$validationResult )
        {
            return redirect()->back()->withInput()->with('errors', $validation->getErrors());
        }
        else
        {
            // get uom from itemno & qty dikali -1
            $item_no = $this->request->getPost('item_no');
            $quantity = $this->request->getPost('quantity');
            $external_document_no = $this->request->getPost('external_document_no');
            $opt = $this->request->getPost('opt');

            // check pn
            $itemModel = new \App\Models\ItemModel();
            $item = $itemModel->where('no', $item_no)->first();
            if ( empty($item) )
            {
                return redirect()->back()->withInput()->with('errors', ['item_no' => lang('Validation.item_not_found')]);
            }

            if ( $opt === 'out' )
            {
                if ( $quantity > $item->soh )
                {
                    return redirect()->back()->withInput()->with('errors', ['item_no' => 'No Stock.']);
                }

                $quantity = $quantity * -1;
            }

            $data = [
                'item_no' => $item_no,
                'quantity' => $quantity,
                'uom' => $item->base_uom,
                'external_document_no' => $external_document_no,
                'user_id' => Services::auth()->id()
            ];

            $entryModel = new \App\Models\LedgerEntryModel();
            $entry = new \App\Entities\LedgerEntry($data);
            if ( !$entryModel->save($entry) )
            {
                log_message('error', implode(" ", $entryModel->errors()));

                return redirect()->back()->withInput();
            }

            return redirect()->back()->with('message', 'success');
        }

    }
}