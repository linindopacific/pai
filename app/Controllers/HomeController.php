<?php

namespace App\Controllers;

use Fluent\Auth\Facades\Auth;

class HomeController extends BaseController
{
    public function index()
    {
        return view('index', [
            'viewLayout' => 'Themes/layout',
            'title' => 'Home'
        ]);
    }

    public function dashboard()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $validation =  \Config\Services::validation();

        return view('dashboard', [
            'viewLayout' => 'Themes/layout',
            'title' => 'Dashboard Entry',
            'validation' => $validation
        ]);
    }
}
