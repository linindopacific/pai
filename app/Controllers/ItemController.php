<?php namespace App\Controllers;

use Fluent\Auth\Facades\Auth;

class ItemController extends BaseController
{
    public function search()
    {
        $q = is_null($this->request->getGet('item_no')) ? '' : $this->request->getGet('item_no');

        $itemModel = new \App\Models\ItemModel();
        $items = $itemModel->item_list($q);

        return $this->response->setJSON($items);
    }

    public function list()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $itemModel = new \App\Models\ItemModel();
        $items = $itemModel->findAll();

        return view('item', [
            'viewLayout' => 'Themes/layout',
            'title' => 'SOH',
            'items' => $items
        ]);
    }
}