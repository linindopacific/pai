<?php namespace App\Models;

use App\Entities\LedgerEntry;
use CodeIgniter\Model;

class LedgerEntryModel extends Model
{
    /**
     * Name of database table
     *
     * @var string
     */
    protected $table = 'ledger_entry';

    /**
     * The format that the results should be returned as.
     * Will be overridden if the as* methods are used.
     *
     * @var Item
     */
    protected $returnType = LedgerEntry::class;

    /**
     * An array of field names that are allowed
     * to be set by the user in inserts/updates.
     *
     * @var array
     */
    protected $allowedFields = [
        'document_no',
        'item_no',
        'description',
        'quantity',
        'uom',
        'qty_per_uom',
        'external_document_no',
        'user_id'
    ];

    /**
     * If true, will set created_at, and updated_at
     * values during insert and update routines.
     *
     * @var boolean
     */
    protected $useTimestamps = true;

    protected $validationRules = [
        'document_no' => [
			'label' => 'Validation.document_no',
			'rules' => 'permit_empty|alpha_numeric_punct|max_length[20]',
			'errors' => []
		],
		'item_no' => [
			'label' => 'Validation.item_no',
			'rules' => 'required|alpha_numeric|exact_length[8]',
			'errors' => []
		],
        'description' => [
			'label' => 'Validation.item_description',
			'rules' => 'permit_empty|string|max_length[50]',
			'errors' => []
		],
        'quantity' => [
			'label' => 'Validation.quantity',
			'rules' => 'required|decimal',
			'errors' => []
		],
        'uom' => [
			'label' => 'Validation.uom',
			'rules' => 'required|alpha_numeric_space|max_length[10]',
			'errors' => []
		],
        'qty_per_uom' => [
			'label' => 'Validation.qty_per_uom',
			'rules' => 'permit_empty|decimal',
			'errors' => []
		],
        'external_document_no' => [
			'label' => 'Validation.external_document_no',
			'rules' => 'permit_empty|string|max_length[35]',
			'errors' => []
		],
        'user_id' => [
			'rules' => 'permit_empty|integer|max_length[10]',
			'errors' => []
        ]
	];

}