<?php namespace App\Models;

use App\Entities\Item;
use CodeIgniter\Model;

class ItemModel extends Model
{
    /**
     * Name of database table
     *
     * @var string
     */
    protected $table = 'item';

    /**
     * The format that the results should be returned as.
     * Will be overridden if the as* methods are used.
     *
     * @var Item
     */
    protected $returnType = Item::class;

    /**
     * An array of field names that are allowed
     * to be set by the user in inserts/updates.
     *
     * @var array
     */
    protected $allowedFields = [
        'no',
        'description',
        'base_uom'
    ];

    /**
     * If true, will set created_at, and updated_at
     * values during insert and update routines.
     *
     * @var boolean
     */
    protected $useTimestamps = true;

    protected $validationRules = [
		'no' => [
			'label' => 'Validation.item_no',
			'rules' => 'required|alpha_numeric|exact_length[8]|is_unique[item.no,id,{id}]',
			'errors' => []
		],
        'description' => [
			'label' => 'Validation.item_description',
			'rules' => 'permit_empty|string|max_length[50]',
			'errors' => []
		],
        'base_uom' => [
			'label' => 'Validation.base_uom',
			'rules' => 'required|alpha_numeric_space|max_length[10]',
			'errors' => []
		],
	];

    public function item_list(string $q = '')
    {
        if ( $q == '' ) {
            $query = $this->orderBy('no')->findAll();
        } else {
            $query = $this->like('no', strtoupper($q))->orderBy('no')->findAll();
        }

        $data = [];
        foreach ( $query as $row ) {
            $data[] = $row->no;
        }

        return $data;
    }
}