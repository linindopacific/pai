<?php namespace App\Rules;

class MyRules
{
    public function check_pn(string $str, string &$error = null): bool
    {
        $itemModel = new \App\Models\ItemModel();
        $item = $itemModel->where('no', $str)->first();
        if ( is_null($item) == true )
        {
            $error = lang('Validation.item_not_found');
            return false;
        }

        return true;
    }

    public function is_ready(string $str, string &$error = null): bool
    {
        $itemModel = new \App\Models\ItemModel();
        $item = $itemModel->where('no', $str)->first();
        if ( is_null($item) )
        {
            $error = lang('Validation.item_not_found');
            return false;
        }

        return true;
    }
}