<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ItemledgerSeeder extends Seeder
{
	public function run()
	{
		$inputFileName = WRITEPATH . 'uploads/item_ledger.xlsx';

		/**  Identify the type of $inputFileName  **/
		$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
		/**  Create a new Reader of the type that has been identified  **/
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		/**  Advise the Reader that we only want to load cell data  **/
		$reader->setReadDataOnly(true);
		/**  Load $inputFileName to a Spreadsheet Object  **/
		$spreadsheet = $reader->load($inputFileName);

        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $entryModel = new \App\Models\LedgerEntryModel();
		$entryModel->skipValidation(true);
        for ($i=1; $i < count($sheetData); $i++) {
            $data = [
                'document_no' => $sheetData[$i]['A'],
                'item_no' => $sheetData[$i]['B'],
                'quantity' => $sheetData[$i]['C'],
                'uom' => $sheetData[$i]['D'],
            ];
            $entry = new \App\Entities\LedgerEntry($data);

            if ( !$entryModel->save($entry) ) {
                log_message('error', implode(" ", $entryModel->errors()));
				throw new \Exception(implode(" ", $entryModel->errors()), 1);
            }
        }
	}
}
