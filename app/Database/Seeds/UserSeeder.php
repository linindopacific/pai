<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
	public function run()
	{
		$inputFileName = WRITEPATH . 'uploads/m_users.xlsx';

		/**  Identify the type of $inputFileName  **/
		$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
		/**  Create a new Reader of the type that has been identified  **/
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		/**  Advise the Reader that we only want to load cell data  **/
		$reader->setReadDataOnly(true);
		/**  Load $inputFileName to a Spreadsheet Object  **/
		$spreadsheet = $reader->load($inputFileName);

        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $userModel = new \App\Models\UserModel();
        for ($i=1; $i < count($sheetData); $i++) {
            $data = [
                'email' => $sheetData[$i]['A'],
                'username' => $sheetData[$i]['B'],
				'password' => 'Linindo90'
            ];

			if (! $userModel->validate(static::rules())) {
				throw new \Exception(implode(" ", $this->validator->getErrors()), 1);
			}

            $user = new \App\Entities\User($data);

            if ( !$userModel->save($user) ) {
                log_message('error', implode(" ", $userModel->errors()));
				throw new \Exception(implode(" ", $userModel->errors()), 1);
            }
        }
	}

	protected static function rules()
    {
        return [
            'username'              => 'required|alpha_numeric_space|min_length[3]|is_unique[users.username]',
            'email'                 => 'required|valid_email|is_unique[users.email]',
            'password'              => 'required|min_length[8]',
            'password_confirmation' => 'matches[password]',
        ];
    }
}
