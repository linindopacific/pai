<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ItemSeeder extends Seeder
{
	public function run()
	{
		$inputFileName = WRITEPATH . 'uploads/m_item.xlsx';

		/**  Identify the type of $inputFileName  **/
		$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
		/**  Create a new Reader of the type that has been identified  **/
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		/**  Advise the Reader that we only want to load cell data  **/
		$reader->setReadDataOnly(true);
		/**  Load $inputFileName to a Spreadsheet Object  **/
		$spreadsheet = $reader->load($inputFileName);

        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $itemModel = new \App\Models\ItemModel();
        for ($i=1; $i < count($sheetData); $i++) {
            $data = [
                'no' => $sheetData[$i]['A'],
                'description' => $sheetData[$i]['B'],
                'base_uom' => $sheetData[$i]['C'],
            ];
            $item = new \App\Entities\Item($data);

            if ( !$itemModel->save($item) ) {
                log_message('error', implode(" ", $itemModel->errors()));
				throw new \Exception(implode(" ", $itemModel->errors()), 1);
            }
        }
	}
}
