<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateItemTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'unsigned' => true,
				'auto_increment' => true,
			],
			'no' => [
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => false
			],
			'description' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => true,
			],
			'base_uom' => [
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => false
			],
			'created_at' => ['type' => 'datetime', 'null' => true],
            'updated_at' => ['type' => 'datetime', 'null' => true],
            'deleted_at' => ['type' => 'datetime', 'null' => true],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('item');
	}

	public function down()
	{
		$this->forge->dropTable('item');
	}
}
