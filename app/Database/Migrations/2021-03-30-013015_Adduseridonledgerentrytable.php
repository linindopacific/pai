<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddUserIdonLedgerEntryTable extends Migration
{
	public function up()
	{
		$fields = [
			'user_id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'null' => true],
		];
		$this->forge->addColumn('ledger_entry', $fields);
	}

	public function down()
	{
		$this->forge->dropColumn('ledger_entry', 'user_id');
	}
}