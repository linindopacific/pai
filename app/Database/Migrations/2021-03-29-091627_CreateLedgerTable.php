<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateLedgerTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'BIGINT',
				'unsigned' => true,
				'auto_increment' => true,
			],
			'document_no' => [
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => true
			],
			'item_no' => [
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => false
			],
			'description' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => true,
			],
			'quantity' => [
				'type' => 'DECIMAL(38,20)',
				'null' => false
			],
			'uom' => [
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => false
			],
			'qty_per_uom' => [
				'type' => 'DECIMAL(38,20)',
				'default' => 1
			],
			'external_document_no' => [
				'type' => 'VARCHAR',
				'constraint' => '35',
				'null' => true
			],
			'created_at' => ['type' => 'datetime', 'null' => true],
            'updated_at' => ['type' => 'datetime', 'null' => true],
            'deleted_at' => ['type' => 'datetime', 'null' => true],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('ledger_entry');
	}

	public function down()
	{
		$this->forge->dropTable('ledger_entry');
	}
}
