<?php

// override core en language system validation or define your own en language validation message
return [
    'item_no' => "Item No.",
    'item_not_found' => 'Item not found.',
    'item_description' => "Item Description",
    'base_uom' => "Base Unit of Measure",
    'uom' => "Unit of Measure",
    'document_no' => "Document No.",
    'external_document_no' => "External Document No.",
    'quantity' => "Quantity (Base)",
    'qty_per_uom' => "Qty. per UoM",
];
