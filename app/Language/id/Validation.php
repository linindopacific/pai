<?php

// override core en language system validation or define your own en language validation message
return [
    'item_no' => "No. Barang",
    'item_not_found' => 'Barang tidak ditemukan.',
    'item_description' => "Deskripsi Barang",
    'base_uom' => "Satuan Dasar Barang",
    'uom' => "Satuan Barang",
    'document_no' => "No. Dokumen",
    'external_document_no' => "No. Dokumen Luar",
    'quantity' => "Kuantitas (Dasar)",
    'qty_per_uom' => "Kuantitas per Satuan",
];
